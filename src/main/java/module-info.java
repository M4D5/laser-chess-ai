module com.dma.laserchess {
    requires static lombok;
    requires com.google.common;
    requires org.apache.logging.log4j;
    requires org.apache.logging.log4j.core;

    requires javafx.controls;
    requires javafx.graphics;

    requires java.desktop;

    exports com.dma.laserchess;
}