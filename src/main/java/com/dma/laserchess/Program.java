package com.dma.laserchess;

import com.dma.laserchess.game.engine.*;
import com.dma.laserchess.game.logic.GameLifecycleManager;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Program extends Application {
    public static Set<String> DEBUG_OPTIONS = new HashSet<>();

    public static void main(String[] args) {
        DEBUG_OPTIONS = Arrays.stream(args).collect(Collectors.toSet());
        launch(args);
    }

    private DrawingContextProvider drawingContextProvider;
    private ScreenViewSelector screenViewSelector;
    private ScreenViewFactory screenViewFactory;
    private ScreenRenderer screenRenderer;
    private ScreenViewLogicTimer screenViewLogicTimer;
    private GameLifecycleManager gameLifecycleManager = new GameLifecycleManager();

    @Override
    public void start(Stage primaryStage) {
        drawingContextProvider = new DrawingContextProvider();

        Group root = new Group();
        DrawingContext dc = drawingContextProvider.getDrawingContext();
        Canvas canvas = new Canvas(dc.getScreenWidth(), dc.getScreenHeight());

        // Needed to capture key events on the canvas https://stackoverflow.com/questions/24126845/javafx-canvas-not-picking-up-key-events
        canvas.addEventFilter(MouseEvent.ANY, (e) -> canvas.requestFocus());

        root.getChildren().add(canvas);

        screenViewSelector = new ScreenViewSelector(canvas);
        screenViewFactory = new ScreenViewFactory(drawingContextProvider, screenViewSelector, gameLifecycleManager);

        screenViewSelector.setCurrentScreen(screenViewFactory.createMainMenuView());
        screenViewLogicTimer = new ScreenViewLogicTimer(screenViewSelector);
        screenViewLogicTimer.start();

        screenRenderer = new ScreenRenderer(canvas.getGraphicsContext2D(), screenViewSelector, drawingContextProvider);
        screenRenderer.start();

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        primaryStage.setTitle("Laser Chess");
        primaryStage.setResizable(false);

        primaryStage.show();

        primaryStage.sizeToScene();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        screenViewLogicTimer.deactivate();
    }
}