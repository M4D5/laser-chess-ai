package com.dma.laserchess.ai;

import com.dma.laserchess.game.logic.Board;
import com.dma.laserchess.game.logic.Direction;
import com.dma.laserchess.game.logic.Orientation;
import com.dma.laserchess.game.logic.Piece;
import com.google.common.collect.Lists;
import com.google.common.collect.Streams;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Log4j2
public class AI {
    private static final int[] RANDOM;

    private int totalMoves;

    private int currentRandom = 0;

    static {
        Random random = new Random();
        RANDOM = new int[1000];

        for (int i = 0; i < RANDOM.length; i++) {
            RANDOM[i] = random.nextInt();
        }
    }

    private final Random random = new Random();

    private Node rootNode = new Node(null);

    private final Board currentGameState;
    private final Board initialGameState;

    @Getter
    private final Piece.PieceColor player;

    public AI(Board currentGameState, Piece.PieceColor player) {
        this.currentGameState = currentGameState;
        this.initialGameState = currentGameState.clone();
        this.player = player;

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        currentGameState.addGameStateUpdatedHandler(a ->

                // Determining the next move takes quite some time, therefore the simulations are carried out on a
                // different thread, since it would otherwise block the UI thread.
                executorService.submit(() -> {
                    try {
                        advanceRootNode(a);

                        if (currentGameState.getPlayerTurn() == player) {
                            executeNextMove();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error(e);
                    }
                }));
    }

    /**
     * java.util.Random is quite show when called as often as in the game simulations, therefore this array is
     * constructed beforehand with random values. As a result it repeats very soon, but is much faster to generate.
     */
    private int getNextRandom() {
        int r = RANDOM[currentRandom++];

        if (currentRandom == RANDOM.length) {
            currentRandom = 0;
        }

        return r;
    }

    public void executeNextMove() {
        Node nextNode = determineNextMove();
        System.out.println("Total Moves: " + totalMoves);
        System.out.println("AI Made move: " + nextNode.action);
        currentGameState.submitTurn(nextNode.action);
    }

    public void advanceRootNode(Board.Action a) {
        Node matchingNode = rootNode.children.get(a);

        if (matchingNode != null) {
            rootNode = matchingNode;
        } else {
            Node newNode = new Node(a);
            rootNode.addChild(newNode);
            rootNode = newNode;
        }
    }

    public Node determineNextMove() {
        if (currentGameState.getPlayerTurn() != player) {
            throw new IllegalStateException("It must be the AI's turn in order to research game states");
        }

        for (int i = 0; i < 100000; i++) {
            researchNewStates();

            if (i % 1000 == 0) {
                System.out.println("Reached iteration: " + i);
            }
        }

        // The next move is chosen based on the highest average utility of the children of the root node (current game
        // state).
        double bestAverageUtility = Integer.MIN_VALUE;
        Node bestNode = null;

        for (Node child : rootNode.children.values()) {
            double averageUtility = (double) child.wonGames / (double) child.totalGames;

            if (averageUtility > bestAverageUtility) {
                bestAverageUtility = averageUtility;
                bestNode = child;
            }
        }

        if (bestNode == null) {
            throw new IllegalStateException();
        }

        return bestNode;
    }

    /**
     * Contains the high-level MCTS Logic, and coordinates the Selection, Expansion, Simulation and Back Propagation.
     */
    private void researchNewStates() {
        // Selection
        Piece.PieceColor turn = player;

        Node currentNode = rootNode;
        Node lastParent = null;

        while (currentNode.totalGames > 0 || currentNode == rootNode) {
            lastParent = currentNode;
            Iterable<Node> expansionCandidates = getExpansionCandidates(currentNode);
            currentNode = determineUcbChildNode(currentNode, expansionCandidates, turn);
            turn = turn.next();
        }

        // Expansion (the new node is added to the tree)
        if (currentNode.parent == null) {
            lastParent.addChild(currentNode);
        } else {
            throw new IllegalStateException("The expanded node was not a new node");
        }

        // Simulation
        Piece.PieceColor winner = simulateGame(currentNode);

        // Back propagate
        boolean won = winner == player;

        while (currentNode.parent != null) {
            currentNode.totalGames++;
            if (won) currentNode.wonGames++;
            currentNode = currentNode.parent;
        }
    }

    // Recovers (or retrieves a cached version of) the board state based on the node.
    private Board getBoardStateForNode(Node node) {
        if (node.state != null) {
            return node.state;
        }

        List<Board.Action> actions = new ArrayList<>();

        Node currentNode = node;

        while (currentNode.parent != null) {
            actions.add(currentNode.action);
            currentNode = currentNode.parent;
        }

        actions = Lists.reverse(actions);

        Board resultBoard = initialGameState.clone();

        for (Board.Action action : actions) {
            resultBoard.submitTurn(action);
        }

        node.state = resultBoard;

        return resultBoard;
    }

    /**
     * Combines the existing nodes with the new, unexplored nodes (if any).
     */
    private Iterable<Node> getExpansionCandidates(Node currentStateNode) {
        CompositeHashMap candidates = new CompositeHashMap(currentStateNode.children);
        addNewChildren(currentStateNode, candidates);
        return candidates.getFullCollection();
    }

    /**
     * Uses the UCB formula to return next node to consider (or new unexplored nodes, any of which will be returned)
     */
    public Node determineUcbChildNode(Node currentStateNode, Iterable<Node> childNodes, Piece.PieceColor turn) {
        int bestUcb = Integer.MIN_VALUE;
        List<Node> highestScoringNodes = new LinkedList<>();

        for (Node node : childNodes) {
            int wonGames;

            if (turn == player) {
                wonGames = node.wonGames;
            } else {
                wonGames = node.totalGames - node.wonGames;
            }

            int ucb;

            if (node.totalGames == 0) {
                // The UCB formula cannot handle nodes without any simulated games, so we set the value to infinity,
                // so that we essentially choose a random node among those that are unexpanded.
                ucb = Integer.MAX_VALUE;
            } else if (node.state.isGameComplete()) {
                ucb = Integer.MIN_VALUE;
            } else {
                ucb = (int) ((wonGames / node.totalGames) +
                        Math.sqrt(2 * Math.log(currentStateNode.totalGames) / node.totalGames));
            }

            if (ucb >= bestUcb) {
                highestScoringNodes.add(node);
                bestUcb = ucb;
            }
        }

        int index;

        if (highestScoringNodes.size() == 1) {
            index = 0;
        } else {
            index = random.nextInt(highestScoringNodes.size() - 1);
        }

        return highestScoringNodes.get(index);
    }

    public Piece.PieceColor simulateGame(Node node) {
        Board simulatedGameState = getBoardStateForNode(node).clone();

        while (!simulatedGameState.isGameComplete()) {
            simulatedGameState.submitTurn(getRandomLegalAction(simulatedGameState));
        }

        return simulatedGameState.getWinner();
    }

    /**
     * Returns a random action from the available legal actions that can currently be made on the board.
     */
    public Board.Action getRandomLegalAction(Board b) {
        List<Piece> pieces;

        if (b.getPlayerTurn() == Piece.PieceColor.RED) {
            pieces = b.getAliveRedPieces();
        } else {
            pieces = b.getAliveWhitePieces();
        }

        // Get a random piece index among
        int piece = Math.abs(getNextRandom() % pieces.size());

        // Use the last index to represent rotating the laser
        if (piece == pieces.size()) {
            return Board.RotateLaser.INSTANCE;
        }

        // Otherwise get the piece at the random index
        Piece p = pieces.get(piece);

        // And get a random action among that pieces valid actions
        Board.Action action = null;

        // To avoid constructing a new list, each valid action has a probability of overwriting the selected action.
        // This saves a substantial amount of time since this function is called so often
        int score = Integer.MIN_VALUE;

        for (Direction d : Direction.values()) {
            Board.Action a = getMovePieceAction(p, d, b);

            if (a != null) {
                int s = getNextRandom();

                if (s > score) {
                    action = a;
                    score = s;
                }
            }
        }

        for (Orientation o : Orientation.values()) {
            Board.Action a = getRotatePieceAction(p, o);

            if (a != null) {
                int s = getNextRandom();

                if (s > score) {
                    action = a;
                    score = s;
                }
            }
        }

        totalMoves++;

        return action;
    }

    /**
     * Adds the new children to the new map in candidates.
     */
    private void addNewChildren(Node node, CompositeHashMap candidates) {
        Board b = getBoardStateForNode(node);

        if (!candidates.originalContainsKey(Board.RotateLaser.INSTANCE)) {
            candidates.put(Board.RotateLaser.INSTANCE, new Node(Board.RotateLaser.INSTANCE));
        }

        List<Piece> pieces;

        if (b.getPlayerTurn() == Piece.PieceColor.RED) {
            pieces = b.getAliveRedPieces();
        } else {
            pieces = b.getAliveWhitePieces();
        }

        for (Piece p : pieces) {
            for (Direction d : Direction.values()) {
                Board.Action a = getMovePieceAction(p, d, b);

                if (a != null && !candidates.originalContainsKey(a)) {
                    candidates.put(a, new Node(a));
                }
            }

            for (Orientation o : Orientation.values()) {
                Board.Action a = getRotatePieceAction(p, o);

                if (a != null && !candidates.originalContainsKey(a)) {
                    candidates.put(a, new Node(a));
                }
            }
        }
    }

    private Board.Action getRotatePieceAction(Piece p, Orientation o) {
        if (Orientation.is90Degrees(p.getOrientation(), o)) {
            return new Board.RotatePiece(p.getX(), p.getY(), o);
        } else {
            return null;
        }
    }

    private Board.Action getMovePieceAction(Piece p, Direction d, Board board) {
        int newX = p.getX() + d.x, newY = p.getY() + d.y;

        if (board.isLegalMove(p, newX, newY)) {
            return new Board.MovePiece(p.getX(), p.getY(), d);
        } else {
            return null;
        }
    }

    @RequiredArgsConstructor
    public static class Node {
        private Board state;
        private final Board.Action action;
        private final Map<Board.Action, Node> children = new HashMap<>(85);

        private Node parent;
        private int wonGames;
        private int totalGames;

        public void addChild(Node node) {
            node.parent = this;
            children.put(node.action, node);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Node)) return false;

            Node node = (Node) o;

            return Objects.equals(action, node.action);
        }

        @Override
        public int hashCode() {
            return action != null ? action.hashCode() : 0;
        }
    }

    /**
     * Container for an existing original map, and a new map, that can be returned as one iterable.
     * This saves the overhead copying the original map.
     */
    @RequiredArgsConstructor
    private static class CompositeHashMap {
        private final Map<Board.Action, Node> originalMap;
        private final Map<Board.Action, Node> newMap = new HashMap<>();

        public boolean originalContainsKey(Board.Action a) {
            return originalMap.containsKey(a);
        }

        public void put(Board.Action key, Node value) {
            newMap.put(key, value);
        }

        public Iterable<Node> getFullCollection() {
            return Streams.concat(originalMap.values().stream(), newMap.values().stream()).collect(Collectors.toList());
        }
    }
}
