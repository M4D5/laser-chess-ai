package com.dma.laserchess.game.logic;

import lombok.ToString;

@ToString(callSuper = true)
public class Defender extends Piece {
    public Defender(PieceColor color, int x, int y, Orientation orientation) {
        super(color, x, y, orientation);
    }

    public Defender(PieceColor color, int x, int y, Orientation orientation, boolean alive)  {
        super(color, x, y, orientation, alive);
    }

    @Override
    public LaserResult handleLaser(Direction d, Board b) {
        if(d.degrees != Orientation.correctDegrees(orientation.getDegrees() + 180)) {
            alive = false;

            if(getColor() == PieceColor.RED) {
                b.getAliveRedPieces().remove(this);
            } else {
                b.getAliveWhitePieces().remove(this);
            }
        }

        return LaserResult.stop();
    }
    public Piece clone() {
        return new Defender(color, x, y, orientation, alive);
    }
}
