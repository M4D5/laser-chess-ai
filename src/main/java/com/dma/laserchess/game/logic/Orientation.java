package com.dma.laserchess.game.logic;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum Orientation {
    NORTH(0),
    EAST(90),
    SOUTH(180),
    WEST(270);

    private final int degrees;

    public Orientation clockwise() {
        return fromDegrees(correctDegrees(degrees + 90));
    }

    public Orientation antiClockwise() {
        return fromDegrees(correctDegrees(degrees - 90));
    }

    public static int correctDegrees(int degrees) {
        int limit = degrees % 360;

        if (limit < 0) {
            limit += 360;
        }

        return limit;
    }

    public static Orientation fromDegrees(int degrees) {
        return Arrays.stream(values())
                .filter(o -> o.getDegrees() == degrees)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No orientation with degrees: " + degrees));
    }

    public static boolean is90Degrees(Orientation o1, Orientation o2) {
        return (180 - Math.abs(Math.abs((o1.degrees) - (o2.degrees)) - 180)) == 90;
    }
}
