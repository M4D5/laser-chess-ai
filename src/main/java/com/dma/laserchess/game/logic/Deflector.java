package com.dma.laserchess.game.logic;

import lombok.ToString;

@ToString(callSuper = true)
public class Deflector extends Piece {
    public Deflector(PieceColor color, int x, int y, Orientation orientation) {
        super(color, x, y, orientation);
    }

    public Deflector(PieceColor color, int x, int y, Orientation orientation, boolean alive) {
        super(color, x, y, orientation, alive);
    }

    @Override
    public LaserResult handleLaser(Direction d, Board b) {
        // In order for the laser to reflect, it must come from the opposite angle of the orientation, or that angle
        // plus 90 degrees
        if(Orientation.correctDegrees(orientation.getDegrees() + 180) == d.degrees) {
            return LaserResult.continueResult(d.reflect(-90));
        }

        if(Orientation.correctDegrees(orientation.getDegrees() + 90 + 180) == d.degrees) {
            return LaserResult.continueResult(d.reflect(90));
        }

        alive = false;

        if(getColor() == PieceColor.RED) {
            b.getAliveRedPieces().remove(this);
        } else {
            b.getAliveWhitePieces().remove(this);
        }

        return LaserResult.stop();
    }
    public Piece clone() {
        return new Deflector(color, x, y, orientation, alive);
    }
}
