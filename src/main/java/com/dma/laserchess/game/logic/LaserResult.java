package com.dma.laserchess.game.logic;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class LaserResult {
    private final boolean shouldContinue;
    private final Direction direction;

    public static LaserResult continueResult(Direction d) {
        return new LaserResult(true, d);
    }

    public static LaserResult stop() {
        return new LaserResult(false, null);
    }
}
