package com.dma.laserchess.game.logic;

import com.dma.laserchess.game.engine.ConsumerEventDelegator;
import lombok.Data;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

@Getter
@Log4j2
public class Laser {
    private final Piece.PieceColor color;
    private int x;
    private int y;
    private Orientation orientation;
    private List<LaserWayPoint> lastPath;

    private ConsumerEventDelegator<List<LaserWayPoint>> fireEventDelegator;

    public Laser(Piece.PieceColor color) {
        this.color = color;

        if (color == Piece.PieceColor.RED) {
            this.x = 0;
            this.y = 0;
            this.orientation = Orientation.SOUTH;
        } else {
            this.x = 9;
            this.y = 7;
            this.orientation = Orientation.NORTH;
        }
    }

    public Laser(Piece.PieceColor color, Orientation orientation) {
        this.orientation = orientation;
        this.color = color;
    }

    void rotate() {
        if (this.color == Piece.PieceColor.RED) {
            if (this.orientation == Orientation.EAST) {
                this.orientation = Orientation.SOUTH;
            } else {
                this.orientation = Orientation.EAST;
            }
        } else {
            if (this.orientation == Orientation.WEST) {
                this.orientation = Orientation.NORTH;
            } else {
                this.orientation = Orientation.WEST;
            }
        }
    }

    private Piece getClosestPiece(Board board, Direction d, int laserX, int laserY) {
        Piece closestPiece = null;

        if (d == Direction.DOWN) {
            for (int i = laserY + 1; i < 8; i++) {
                Piece p = board.getPiecesGrid()[i][laserX];
                if (p != null && p.isAlive()) {
                    closestPiece = p;
                    break;
                }
            }
        } else if (d == Direction.UP) {
            for (int i = laserY - 1; i >= 0; i--) {
                Piece p = board.getPiecesGrid()[i][laserX];
                if (p != null && p.isAlive()) {
                    closestPiece = p;
                    break;
                }
            }
        } else if (d == Direction.RIGHT) {
            for (int i = laserX + 1; i < 10; i++) {
                Piece p = board.getPiecesGrid()[laserY][i];
                if (p != null && p.isAlive()) {
                    closestPiece = p;
                    break;
                }
            }
        } else if (d == Direction.LEFT) {
            for (int i = laserX - 1; i >= 0; i--) {
                Piece p = board.getPiecesGrid()[laserY][i];
                if (p != null && p.isAlive()) {
                    closestPiece = p;
                    break;
                }
            }
        } else {
            throw new IllegalArgumentException("Illegal direction " + d);
        }

        return closestPiece;
    }

    public void fire(Board board) {
        List<LaserWayPoint> path = new LinkedList<>();

        int laserX = x;
        int laserY = y;

        boolean shouldContinue = true;
        Direction currentDirection = Direction.fromDegrees(orientation.getDegrees());

        while (shouldContinue) {
            Piece closestPiece = getClosestPiece(board, currentDirection, laserX, laserY);

            if (closestPiece == null) {
                break;
            }

            laserX = closestPiece.x;
            laserY = closestPiece.y;

            LaserResult laserResult = closestPiece.handleLaser(currentDirection, board);
            currentDirection = laserResult.getDirection();
            shouldContinue = laserResult.isShouldContinue();

            path.add(new LaserWayPoint(closestPiece.x, closestPiece.y, laserResult));
        }

        this.lastPath = path;

        if(fireEventDelegator != null) {
            fireEventDelegator.handle(path);
        }
    }

    public void subscribeToFire(Consumer<List<LaserWayPoint>> consumer) {
        if(fireEventDelegator == null) {
            fireEventDelegator = new ConsumerEventDelegator<>();
        }

        fireEventDelegator.addHandler(consumer);
    }

    @Data
    public static class LaserWayPoint {
        private final int x;
        private final int y;
        private final LaserResult result;
    }

    public Laser clone() {
        return new Laser(color, orientation);
    }
}
