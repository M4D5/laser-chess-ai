package com.dma.laserchess.game.logic;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import lombok.Getter;
import lombok.SneakyThrows;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class GameLifecycleManager {
    @Getter
    private Board board;

    @SneakyThrows(IOException.class)
    public void newGame() {
        BoardCSVReader csvReader = new BoardCSVReader();

        String csv;

        try (InputStream is = GameLifecycleManager.class.getResourceAsStream("/ace.csv")) {
            csv = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
        }

        List<Piece> pieces = csvReader.parseBoard(csv);
        board = new Board(pieces);
    }
}
