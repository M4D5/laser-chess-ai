package com.dma.laserchess.game.logic;

import lombok.ToString;

@ToString(callSuper = true)
public class Switch extends Piece {
    public Switch(PieceColor color, int x, int y, Orientation orientation) {
        super(color, x, y, orientation);
    }

    public Switch(PieceColor color, int x, int y, Orientation orientation, boolean alive) {
        super(color, x, y, orientation, alive);
    }

    @Override
    public void move(Board.MovePiece movePiece, Board b) {
        b.getPiecesGrid()[y][x] = null;
        this.x += movePiece.getTargetDirection().x;
        this.y += movePiece.getTargetDirection().y;

        Piece switchPiece = b.getPiecesGrid()[y][x];

        if (switchPiece != null) {
            switchPiece.x -= movePiece.getTargetDirection().x;
            switchPiece.y -= movePiece.getTargetDirection().y;
            b.getPiecesGrid()[switchPiece.y][switchPiece.x] = switchPiece;
        }

        b.getPiecesGrid()[y][x] = this;
    }

    @Override
    public LaserResult handleLaser(Direction d, Board b) {
        if(Orientation.correctDegrees(orientation.getDegrees() + 180) == d.degrees) {
            return LaserResult.continueResult(d.reflect(-90));
        } else if(Orientation.correctDegrees(orientation.getDegrees() + 90 + 180) == d.degrees) {
            return LaserResult.continueResult(d.reflect(90));
        } else  if(Orientation.correctDegrees(orientation.getDegrees()) == d.degrees) {
            return LaserResult.continueResult(d.reflect(-90));
        } else if(Orientation.correctDegrees(orientation.getDegrees() + 90) == d.degrees) {
            return LaserResult.continueResult(d.reflect(90));
        }

        throw new IllegalStateException();
    }

    public Piece clone() {
        return new Switch(color, x, y, orientation, alive);
    }
}
