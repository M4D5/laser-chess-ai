package com.dma.laserchess.game.logic;

import com.google.common.base.Strings;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log4j2
public class BoardCSVReader {
    private static final Pattern PATTERN = Pattern.compile("([WR]) (SWITCH|KING|DEFENDER|DEFLECTOR) ([NESW])");

    public List<Piece> parseBoard(String csv) {
        String[] rowStrings = csv.split("\r\n");
        String[][] cellStrings = Arrays.stream(rowStrings).map(s -> s.split(",")).toArray(String[][]::new);

        List<Piece> pieces = new ArrayList<>();

        for (int y = 0; y < 10; y++) {
            if(y > cellStrings.length - 1) {
                break;
            }

            for (int x = 0; x < 10; x++) {
                if(x > cellStrings[y].length - 1) {
                    break;
                }

                String str = cellStrings[y][x];

                if (Strings.isNullOrEmpty(str)) {
                    continue;
                }

                Matcher matcher = PATTERN.matcher(str);

                if (!matcher.find()) {
                    log.warn(String.format("Invalid format for string: '%s' in position (%d, %d)", str, x, y));
                    continue;
                }

                pieces.add(createPiece(matcher.group(1), matcher.group(2), matcher.group(3), x, y));
            }
        }

        return pieces;
    }

    private Piece createPiece(String color, String pieceType, String orientation, int x, int y) {
        boolean white = color.equals("W");
        Orientation o = getOrientation(orientation);

        switch (pieceType) {
            case "SWITCH":
                return new Switch(white ? Piece.PieceColor.WHITE : Piece.PieceColor.RED, x, y, o);
            case "KING":
                return new King(white ? Piece.PieceColor.WHITE : Piece.PieceColor.RED, x, y, o);
            case "DEFENDER":
                return new Defender(white ? Piece.PieceColor.WHITE : Piece.PieceColor.RED, x, y, o);
            case "DEFLECTOR":
                return new Deflector(white ? Piece.PieceColor.WHITE : Piece.PieceColor.RED, x, y, o);
            default:
                throw new IllegalStateException("Invalid piece type: '" + pieceType + "'");
        }
    }

    private Orientation getOrientation(String str) {
        switch (str) {
            case "N":
                return Orientation.NORTH;
            case "E":
                return Orientation.EAST;
            case "S":
                return Orientation.SOUTH;
            case "W":
                return Orientation.WEST;
            default:
                throw new IllegalStateException("Invalid orientation '" + str + "'");
        }
    }
}
