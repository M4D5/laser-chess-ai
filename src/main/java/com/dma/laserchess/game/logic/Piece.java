package com.dma.laserchess.game.logic;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public abstract class Piece {
    protected final PieceColor color;
    protected int x;
    protected int y;
    protected Orientation orientation;
    protected boolean alive = true;

    public Piece(PieceColor color, int x, int y, Orientation orientation) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public Piece(PieceColor color, int x, int y, Orientation orientation, boolean alive) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.orientation = orientation;
        this.alive = alive;
    }

    public abstract LaserResult handleLaser(Direction d, Board b);

    public Board.RotatePiece getAction(Orientation orientation, Board b) {
        if (!Orientation.is90Degrees(orientation, this.orientation)) {
            throw new IllegalStateException("Illegal move from orientation " + orientation + " from orientation " + this.orientation);
        }

        return new Board.RotatePiece(x, y, orientation);
    }

    void move(Board.RotatePiece action) {
        this.orientation = action.getTargetOrientation();
    }

    public Board.MovePiece getAction(Direction d, Board b) {
        int newX = this.x + d.x, newY = this.y + d.y;

        if (!b.isLegalMove(this, newX, newY)) {
            throw new IllegalStateException(String.format("Illegal move to position (%s, %s)", newX, newY));
        }

        return new Board.MovePiece(x, y, d);
    }

    public void move(Board.MovePiece movePiece, Board b) {
        b.getPiecesGrid()[y][x] = null;
        this.x += movePiece.getTargetDirection().x;
        this.y += movePiece.getTargetDirection().y;
        b.getPiecesGrid()[y][x] = this;
    }

    public enum PieceColor {
        WHITE,
        RED;

        public PieceColor next() {
            if (this == WHITE) {
                return RED;
            } else {
                return WHITE;
            }
        }
    }

    public abstract Piece clone();
}
