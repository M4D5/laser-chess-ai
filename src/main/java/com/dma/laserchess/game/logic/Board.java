package com.dma.laserchess.game.logic;

import com.dma.laserchess.game.engine.ConsumerEventDelegator;
import lombok.*;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.dma.laserchess.game.logic.Board.SpaceType.*;

@Getter
public class Board {
    private static final SpaceType[][] BOARD_LAYOUT = {
            {INACCESSIBLE, WHITE_ONLY, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, RED_ONLY, WHITE_ONLY},
            {RED_ONLY, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, WHITE_ONLY},
            {RED_ONLY, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, WHITE_ONLY},
            {RED_ONLY, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, WHITE_ONLY},
            {RED_ONLY, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, WHITE_ONLY},
            {RED_ONLY, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, WHITE_ONLY},
            {RED_ONLY, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, WHITE_ONLY},
            {RED_ONLY, WHITE_ONLY, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, RED_ONLY, INACCESSIBLE},
    };

    private final Piece[][] piecesGrid;
    private final List<Piece> aliveRedPieces;
    private final List<Piece> aliveWhitePieces;

    private Piece.PieceColor playerTurn = Piece.PieceColor.WHITE;

    private final Laser whiteLaser;
    private final Laser redLaser;

    private boolean gameComplete;
    private Piece.PieceColor winner;

    private final ConsumerEventDelegator<Action> gameStateUpdatedEventDelegator = new ConsumerEventDelegator<>();

    public Board(List<Piece> pieces) {
        this.piecesGrid = new Piece[8][10];
        pieces.forEach(p -> piecesGrid[p.getY()][p.getX()] = p);
        aliveRedPieces = pieces.stream().filter(Piece::isAlive).filter(p -> p.getColor() == Piece.PieceColor.RED).collect(Collectors.toList());
        aliveWhitePieces = pieces.stream().filter(Piece::isAlive).filter(p -> p.getColor() == Piece.PieceColor.WHITE).collect(Collectors.toList());

        this.whiteLaser = new Laser(Piece.PieceColor.WHITE);
        this.redLaser = new Laser(Piece.PieceColor.RED);
    }

    public Board(List<Piece> pieces, Piece.PieceColor playerTurn, Laser redLaser, Laser whiteLaser) {
        this.piecesGrid = new Piece[8][10];
        pieces.forEach(p -> piecesGrid[p.getY()][p.getX()] = p);

        aliveRedPieces = pieces.stream().filter(Piece::isAlive).filter(p -> p.getColor() == Piece.PieceColor.RED).collect(Collectors.toList());
        aliveWhitePieces = pieces.stream().filter(Piece::isAlive).filter(p -> p.getColor() == Piece.PieceColor.WHITE).collect(Collectors.toList());

        this.playerTurn = playerTurn;
        this.redLaser = redLaser;
        this.whiteLaser = whiteLaser;
    }

    public void addGameStateUpdatedHandler(Consumer<Action> handler) {
        gameStateUpdatedEventDelegator.addHandler(handler);
    }

    /**
     * Checks whether a move to the destination (x, y) is a legal move
     *
     * @param p the piece to be moved
     * @param x the destination x coordinate
     * @param y the destination y coordinate
     * @return true, if the move is legal, false otherwise
     */
    public boolean isLegalMove(Piece p, int x, int y) {
        if (p.getColor() != playerTurn) {
            return false;
        }

        if (x < 0 || x > 9 || y < 0 || y > 7) {
            return false;
        }

        Piece piece = piecesGrid[y][x];

        if (piece != null && piece.isAlive()) {
            return p instanceof Switch &&
                    ((piece instanceof Deflector) ||
                    (piece instanceof Defender));
        }

        return BOARD_LAYOUT[y][x].isLegalForPieceColor(p.getColor());
    }

    public void submitTurn(Action a) {
        doMove(a);

        if (playerTurn == Piece.PieceColor.WHITE) {
            whiteLaser.fire(this);
        } else {
            redLaser.fire(this);
        }

        playerTurn = playerTurn.next();

        gameStateUpdatedEventDelegator.handle(a);
    }

    private void doMove(Action action) {
        if (action instanceof PieceAction) {
            PieceAction pieceAction = (PieceAction) action;

            Piece p = piecesGrid[pieceAction.pieceY][pieceAction.pieceX];

            if (p == null || !(p.isAlive() && p.getColor() == playerTurn)) {
                throw new IllegalStateException("Action refers to non-existent piece");
            }

            if (pieceAction instanceof MovePiece) {
                p.move(((MovePiece) pieceAction), this);
            } else {
                p.move(((RotatePiece) pieceAction));
            }
        } else if (action instanceof RotateLaser) {
            if (playerTurn == Piece.PieceColor.WHITE) {
                whiteLaser.rotate();
            } else {
                redLaser.rotate();
            }
        }
    }

    public List<Piece> getPieces() {
        List<Piece> pieces = new ArrayList<>();

        for (Piece[] piecesRow : piecesGrid) {
            for (Piece p : piecesRow) {
                if (p != null) {
                    pieces.add(p);
                }
            }
        }

        return pieces;
    }

    public Optional<Piece> getPieceAtPos(int x, int y) {
        return Optional.ofNullable(piecesGrid[y][x]);
    }

    public void endGame(Piece.PieceColor winner) {
        this.winner = winner;
        gameComplete = true;
    }

    @AllArgsConstructor
    public enum SpaceType {
        NORMAL,
        RED_ONLY,
        WHITE_ONLY,
        INACCESSIBLE;

        public boolean isLegalForPieceColor(Piece.PieceColor p) {
            switch (this) {
                case NORMAL:
                    return true;
                case INACCESSIBLE:
                    return false;
                case RED_ONLY:
                    return p == Piece.PieceColor.RED;
                case WHITE_ONLY:
                    return p == Piece.PieceColor.WHITE;
            }

            throw new IllegalStateException();
        }
    }

    public interface Action {
    }

    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @EqualsAndHashCode
    public static class RotateLaser implements Action {
        public static final RotateLaser INSTANCE = new RotateLaser();

        @Override
        public String toString() {
            return "RotateLaser";
        }
    }

    @Getter
    @AllArgsConstructor
    @EqualsAndHashCode
    public static abstract class PieceAction implements Action {
        private final int pieceX;
        private final int pieceY;
    }

    @Getter
    @EqualsAndHashCode(callSuper = true)
    public static class RotatePiece extends PieceAction {
        private final Orientation targetOrientation;

        public RotatePiece(int pieceX, int pieceY, Orientation targetOrientation) {
            super(pieceX, pieceY);
            this.targetOrientation = targetOrientation;
        }

        @Override
        public String toString() {
            return String.format("RotatePiece{(%d, %d), %s}", getPieceX(), getPieceY(), targetOrientation);
        }
    }

    @Getter
    @EqualsAndHashCode(callSuper = true)
    public static class MovePiece extends PieceAction {
        private final Direction targetDirection;

        public MovePiece(int pieceX, int pieceY, Direction targetDirection) {
            super(pieceX, pieceY);
            this.targetDirection = targetDirection;
        }

        @Override
        public String toString() {
            return String.format("MovePiece{(%d, %d), %s}", getPieceX(), getPieceY(), targetDirection);
        }
    }

    public Board clone() {
        List<Piece> clonedPieces = new ArrayList<>();

        for (Piece[] piecesRow : piecesGrid) {
            for (Piece p : piecesRow) {
                if (p != null) {
                    clonedPieces.add(p.clone());
                }
            }
        }

        Board board = new Board(clonedPieces, playerTurn, redLaser.clone(), whiteLaser.clone());

        board.gameComplete = gameComplete;
        board.winner = winner;

        return board;
    }
}
