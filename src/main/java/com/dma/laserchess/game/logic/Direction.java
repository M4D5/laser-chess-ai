package com.dma.laserchess.game.logic;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Direction {
    UP(0, -1, 0, "up"),
    UP_RIGHT(1, -1, 45, "up right"),
    RIGHT(1, 0, 90, "right"),
    DOWN_RIGHT(1, 1, 135, "down right"),
    DOWN(0, 1, 180, "down"),
    DOWN_LEFT(-1, 1, 225, "down left"),
    LEFT(-1, 0, 270, "left"),
    UP_LEFT(-1, -1, 315, "up left");

    public final int x;
    public final int y;
    public final int degrees;
    public final String displayName;

    public Direction reflect(int degrees) {
        int reflected = (degrees + this.degrees) % 360;

        if (reflected < 0) {
            reflected += 360;
        }

        return Direction.fromDegrees(reflected);
    }

    public static Direction fromDegrees(int degrees) {
        switch (degrees) {
            case 0:
                return UP;
            case 45:
                return UP_RIGHT;
            case 90:
                return RIGHT;
            case 135:
                return DOWN_RIGHT;
            case 180:
                return DOWN;
            case 225:
                return DOWN_LEFT;
            case 270:
                return LEFT;
            case 315:
                return UP_LEFT;
            default:
                throw new IllegalStateException("There is no direction with degrees " + degrees);
        }
    }
}
