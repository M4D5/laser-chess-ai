package com.dma.laserchess.game.logic;

import lombok.ToString;

@ToString(callSuper = true)
public class King extends Piece {
    public King(PieceColor color, int x, int y, Orientation orientation) {
        super(color, x, y, orientation);
    }

    public King(PieceColor color, int x, int y, Orientation orientation, boolean alive) {
        super(color, x, y, orientation, alive);
    }

    @Override
    public LaserResult handleLaser(Direction d, Board b) {
        if(b.getPlayerTurn() == color) {
            b.endGame(b.getPlayerTurn().next());
        } else {
            b.endGame(color.next());
        }


        alive = false;

        if(getColor() == PieceColor.RED) {
            b.getAliveRedPieces().remove(this);
        } else {
            b.getAliveWhitePieces().remove(this);
        }

        return LaserResult.stop();
    }
    public Piece clone() {
        return new King(color, x, y, orientation, alive);
    }
}
