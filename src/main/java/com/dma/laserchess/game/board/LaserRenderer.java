package com.dma.laserchess.game.board;

import com.dma.laserchess.game.engine.Drawable;
import com.dma.laserchess.game.engine.DrawingContext;
import com.dma.laserchess.game.engine.PixelArtRenderer;
import com.dma.laserchess.game.engine.Updatable;
import com.dma.laserchess.game.logic.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.awt.*;

public class LaserRenderer implements Drawable, Updatable {
    private static final Image RED_LASER = new Image(PieceRenderer.class.getResourceAsStream("/red/laser.png"));
    private static final Image WHITE_LASER = new Image(PieceRenderer.class.getResourceAsStream("/white/laser.png"));

    private static final int BOARD_PADDING = 9;
    private static final int BOARD_SPACE_DIMENSION = 26;

    private final Laser laser;
    private final int boardX;
    private final int boardY;
    private final Image img;

    private final int spaceX;
    private final int spaceY;

    private final int x;
    private final int y;

    private boolean active;
    private int activeTimer;

    public LaserRenderer(Laser laser, int boardX, int boardY) {
        this.laser = laser;
        this.boardX = boardX;
        this.boardY = boardY;
        this.img = getPieceImage();

        laser.subscribeToFire(laserWayPoints -> {
            active = true;
            activeTimer = 120;
        });

        if (laser.getColor() == Piece.PieceColor.WHITE) {
            spaceX = 9;
            spaceY = 7;
        } else {
            spaceX = 0;
            spaceY = 0;
        }

        x = boardX + BOARD_PADDING + spaceX * BOARD_SPACE_DIMENSION;
        y = boardY + BOARD_PADDING + spaceY * BOARD_SPACE_DIMENSION;
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        if (active && laser.getLastPath() != null) {
            drawLaser(gc, dc);
        }

        PixelArtRenderer.render(
                img,
                new Point(x, y),
                laser.getOrientation().getDegrees(),
                dc,
                gc);
    }

    private void drawLaser(GraphicsContext gc, DrawingContext dc) {
        int lastX = spaceX, lastY = spaceY;
        Direction lastDirection = Direction.fromDegrees(laser.getOrientation().getDegrees());
        LaserResult lastResult = null;

        for (Laser.LaserWayPoint wayPoint : laser.getLastPath()) {
            drawLaserBeam(gc, dc, lastX, lastY, wayPoint.getX(), wayPoint.getY(), lastDirection);

            lastX = wayPoint.getX();
            lastY = wayPoint.getY();
            lastDirection = wayPoint.getResult().getDirection();
            lastResult = wayPoint.getResult();
        }

        if (lastResult == null || lastResult.isShouldContinue()) {
            int destX, destY;

            if (lastDirection == Direction.UP) {
                destX = lastX;
                destY = -1; // Draw one space further than off the board
            } else if (lastDirection == Direction.DOWN) {
                destX = lastX;
                destY = 8;
            } else if (lastDirection == Direction.LEFT) {
                destX = -1;
                destY = lastY;
            } else {
                destX = 10;
                destY = lastY;
            }

            drawLaserBeam(gc, dc, lastX, lastY, destX, destY, lastDirection);
        }
    }

    private void drawLaserBeam(GraphicsContext gc, DrawingContext dc, int srcSpaceX, int srcSpaceY, int destSpaceX, int destSpaceY, Direction d) {
        int width = Math.abs(destSpaceX - srcSpaceX);
        int height = Math.abs(destSpaceY - srcSpaceY);

        int leftCornerX;
        int leftCornerY;

        if (d == Direction.DOWN || d == Direction.RIGHT) {
            leftCornerX = boardX + BOARD_PADDING + srcSpaceX * BOARD_SPACE_DIMENSION;
            leftCornerY = boardY + BOARD_PADDING + srcSpaceY * BOARD_SPACE_DIMENSION;
        } else {
            leftCornerX = boardX + BOARD_PADDING + destSpaceX * BOARD_SPACE_DIMENSION;
            leftCornerY = boardY + BOARD_PADDING + destSpaceY * BOARD_SPACE_DIMENSION;
        }

        int outerLayerOffset = 9;
        int innerLayerOffset = 10;

        int sf = dc.getScalingFactor();

        if (d == Direction.RIGHT || d == Direction.LEFT) {
            int offsetWidth = width * BOARD_SPACE_DIMENSION;

            gc.setFill(Color.RED);
            gc.fillRect((leftCornerX + outerLayerOffset) * sf, (leftCornerY + outerLayerOffset) * sf, offsetWidth * sf, 3 * sf);

            gc.setFill(Color.PINK);
            gc.fillRect((leftCornerX + innerLayerOffset) * sf, (leftCornerY + innerLayerOffset) * sf, offsetWidth * sf, sf);
        } else if (d == Direction.DOWN || d == Direction.UP) {
            int offsetHeight = height * BOARD_SPACE_DIMENSION;

            gc.setFill(Color.RED);
            gc.fillRect((leftCornerX + outerLayerOffset) * sf, (leftCornerY + outerLayerOffset) * sf, 3 * sf, offsetHeight * sf);

            gc.setFill(Color.PINK);
            gc.fillRect((leftCornerX + innerLayerOffset) * sf, (leftCornerY + innerLayerOffset) * sf, sf, offsetHeight * sf);
        }
    }

    @Override
    public void update(long updateCount) {
        if (active && activeTimer > 0) {
            activeTimer--;

            if (activeTimer == 0) {
                active = false;
            }
        }
    }

    private Image getPieceImage() {
        if (laser.getColor() == Piece.PieceColor.WHITE) {
            return WHITE_LASER;
        } else {
            return RED_LASER;
        }
    }
}
