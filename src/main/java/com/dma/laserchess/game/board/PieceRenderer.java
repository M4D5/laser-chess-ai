package com.dma.laserchess.game.board;

import com.dma.laserchess.game.engine.Drawable;
import com.dma.laserchess.game.engine.DrawingContext;
import com.dma.laserchess.game.engine.PixelArtRenderer;
import com.dma.laserchess.game.logic.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.awt.*;

public class PieceRenderer implements Drawable {
    private static final Image RED_SWITCH = new Image(PieceRenderer.class.getResourceAsStream("/red/switch.png"));
    private static final Image WHITE_SWITCH = new Image(PieceRenderer.class.getResourceAsStream("/white/switch.png"));

    private static final Image RED_DEFLECTOR = new Image(PieceRenderer.class.getResourceAsStream("/red/deflector.png"));
    private static final Image WHITE_DEFLECTOR = new Image(PieceRenderer.class.getResourceAsStream("/white/deflector.png"));

    private static final Image RED_DEFENDER = new Image(PieceRenderer.class.getResourceAsStream("/red/defender.png"));
    private static final Image WHITE_DEFENDER = new Image(PieceRenderer.class.getResourceAsStream("/white/defender.png"));

    private static final Image RED_KING = new Image(PieceRenderer.class.getResourceAsStream("/red/king.png"));
    private static final Image WHITE_KING = new Image(PieceRenderer.class.getResourceAsStream("/white/king.png"));

    private static final int BOARD_PADDING = 9;
    private static final int BOARD_SPACE_DIMENSION = 26;

    private final Piece piece;
    private final int boardX;
    private final int boardY;
    private final Image img;
    private final Point pos = new Point();

    public PieceRenderer(Piece piece, int boardX, int boardY) {
        this.piece = piece;
        this.boardX = boardX;
        this.boardY = boardY;
        this.img = getPieceImage();
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        if(!piece.isAlive()) {
            return;
        }

        pos.x = boardX + BOARD_PADDING + piece.getX() * BOARD_SPACE_DIMENSION;
        pos.y = boardY + BOARD_PADDING + piece.getY() * BOARD_SPACE_DIMENSION;

        PixelArtRenderer.render(
                img,
                pos,
                piece.getOrientation().getDegrees(),
                dc,
                gc);
    }

    private Image getPieceImage() {
        if (piece.getClass().equals(Switch.class)) {
            if (piece.getColor() == Piece.PieceColor.WHITE) {
                return WHITE_SWITCH;
            } else {
                return RED_SWITCH;
            }
        } else if (piece.getClass().equals(Deflector.class)) {
            if (piece.getColor() == Piece.PieceColor.WHITE) {
                return WHITE_DEFLECTOR;
            } else {
                return RED_DEFLECTOR;
            }
        } else if (piece.getClass().equals(King.class)) {
            if (piece.getColor() == Piece.PieceColor.WHITE) {
                return WHITE_KING;
            } else {
                return RED_KING;
            }
        } else if (piece.getClass().equals(Defender.class)) {
            if (piece.getColor() == Piece.PieceColor.WHITE) {
                return WHITE_DEFENDER;
            } else {
                return RED_DEFENDER;
            }
        } else {
            throw new IllegalStateException("Unrecognized piece: " + piece.getClass().getSimpleName());
        }
    }
}
