package com.dma.laserchess.game.board;

import com.dma.laserchess.ai.AI;
import com.dma.laserchess.game.engine.*;
import com.dma.laserchess.game.engine.components.*;
import com.dma.laserchess.game.logic.Board;
import com.dma.laserchess.game.logic.Direction;
import com.dma.laserchess.game.logic.GameLifecycleManager;
import com.dma.laserchess.game.logic.Piece;
import com.google.common.collect.Lists;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.util.List;
import java.util.Optional;

@Log4j2
public class BoardScreen implements ScreenView {
    private static final Image BOARD = new Image(BoardScreen.class.getResourceAsStream("/board.png"));

    private final JavaFXEventDelegator<MouseEvent> mouseMoveEventDelegator = new JavaFXEventDelegator<>();
    private final JavaFXEventDelegator<MouseEvent> mouseClickEventDelegator = new JavaFXEventDelegator<>();

    private final GameLifecycleManager gameLifecycleManager;
    private final DrawingContextProvider drawingContextProvider;
    private final Point boardPos;
    private final List<Drawable> drawables;
    private final List<Updatable> updatables;

    private final Text playerTurnText;

    private final TextButton up;
    private final TextButton upLeft;
    private final TextButton upRight;
    private final TextButton down;
    private final TextButton downLeft;
    private final TextButton downRight;
    private final TextButton left;
    private final TextButton right;
    private final TextButton rotateCW;
    private final TextButton rotateAntiCW;
    private final TextButton rotateLaser;


    private final LaserRenderer whiteLaserRenderer;
    private final LaserRenderer redLaserRenderer;

    private Piece selectedPiece;

    private final AI ai;

    public BoardScreen(GameLifecycleManager gameLifecycleManager, DrawingContextProvider drawingContextProvider) {
        this.gameLifecycleManager = gameLifecycleManager;
        this.drawingContextProvider = drawingContextProvider;

        int width = BOARD.widthProperty().intValue();
        DrawingContext drawingContext = drawingContextProvider.getDrawingContext();
        boardPos = new Point(drawingContext.getWidth() / 2 - width / 2, 15);

        gameLifecycleManager.newGame();
        ai = new AI(gameLifecycleManager.getBoard(), Piece.PieceColor.RED);

        drawables = Lists.newArrayList(
                // Background
                (gc, updateCount, dc) -> {
                    gc.setFill(Color.BLACK);
                    gc.fillRect(0, 0, dc.getScreenWidth(), dc.getScreenHeight());
                },
                // Board
                (gc, updateCount, dc) -> PixelArtRenderer.render(BOARD, boardPos, dc, gc)
        );

        redLaserRenderer = new LaserRenderer(gameLifecycleManager.getBoard().getRedLaser(), boardPos.x, boardPos.y);
        whiteLaserRenderer = new LaserRenderer(gameLifecycleManager.getBoard().getWhiteLaser(), boardPos.x, boardPos.y);

        drawables.add(redLaserRenderer);
        drawables.add(whiteLaserRenderer);

        updatables = Lists.newArrayList(redLaserRenderer, whiteLaserRenderer);

        gameLifecycleManager.getBoard().getPieces()
                .stream()
                .map(p -> new PieceRenderer(p, boardPos.x, boardPos.y))
                .forEach(drawables::add);

        playerTurnText = new Text("", new Position(Anchor.TOP_LEFT, false, false, 13, 4));
        drawables.add(playerTurnText);
        updatePlayerTurnText();

        int center = drawingContext.getWidth() / 2;

        up = createMovementButton(Direction.UP, Position.centeredX(250));
        upLeft = createMovementButton(Direction.UP_LEFT, centerWithOffset(-70, 250));
        upRight = createMovementButton(Direction.UP_RIGHT, centerWithOffset(70, 250));

        left = createMovementButton(Direction.LEFT, centerWithOffset(-70, 260));
        right = createMovementButton(Direction.RIGHT, centerWithOffset(70, 260));

        down = createMovementButton(Direction.DOWN, Position.centeredX(270));
        downLeft = createMovementButton(Direction.DOWN_LEFT, centerWithOffset(-70, 270));
        downRight = createMovementButton(Direction.DOWN_RIGHT, centerWithOffset(70, 270));

        rotateLaser = new TextButton("rotate laser", new Position(Anchor.TOP_CENTER, false, false, center, 290), drawingContextProvider)
                .withAction(() -> {
                    selectedPiece = null;
                    gameLifecycleManager.getBoard().submitTurn(Board.RotateLaser.INSTANCE);
                    updateValidMoves();
                    updatePlayerTurnText();
                });

        drawables.add(rotateLaser);
        mouseMoveEventDelegator.addHandler(rotateLaser.getMouseMoveListener());
        mouseClickEventDelegator.addHandler(rotateLaser.getMouseClickListener());

        rotateAntiCW = new TextButton("rotate anti cw", new Position(Anchor.TOP_LEFT, false, false, center - 120, 290), drawingContextProvider)
                .withAction(() -> {
                    if (selectedPiece != null) {
                        gameLifecycleManager.getBoard()
                                .submitTurn(selectedPiece.getAction(selectedPiece.getOrientation().antiClockwise(), gameLifecycleManager.getBoard()));
                        selectedPiece = null;
                        updateValidMoves();
                        updatePlayerTurnText();
                    }
                });

        rotateCW = new TextButton("rotate cw", new Position(Anchor.TOP_RIGHT, false, false, center + 120, 290), drawingContextProvider)
                .withAction(() -> {
                    if (selectedPiece != null) {
                        gameLifecycleManager.getBoard()
                                .submitTurn(selectedPiece.getAction(selectedPiece.getOrientation().clockwise(), gameLifecycleManager.getBoard()));
                        selectedPiece = null;
                        updateValidMoves();
                        updatePlayerTurnText();
                    }
                });

        drawables.add(rotateAntiCW);
        drawables.add(rotateCW);

        rotateCW.setDisabled(true);
        rotateAntiCW.setDisabled(true);

        mouseMoveEventDelegator.addHandler(rotateAntiCW.getMouseMoveListener());
        mouseMoveEventDelegator.addHandler(rotateCW.getMouseMoveListener());
        mouseClickEventDelegator.addHandler(rotateAntiCW.getMouseClickListener());
        mouseClickEventDelegator.addHandler(rotateCW.getMouseClickListener());

        // Selection listener
        mouseClickEventDelegator.addHandler(e -> {
            int sf = drawingContext.getScalingFactor();
            int x = (int) ((e.getX() / sf - 7 - boardPos.x) / 26);
            int y = (int) ((e.getY() / sf - 7 - boardPos.y) / 26);

            if(x >= 0 && x <= 9 && y >= 0 && y <= 7) {
                Optional<Piece> piece = gameLifecycleManager.getBoard().getPieceAtPos(x, y);

                if(piece.isPresent() && piece.get().getColor() == gameLifecycleManager.getBoard().getPlayerTurn()) {
                    setSelectedPiece(piece.get());
                }
            }
        });

        // Selection highlight
        drawables.add(((gc, updateCount, dc) -> {
            if (selectedPiece != null) {
                gc.setFill(Color.BLUE);

                int sf = drawingContext.getScalingFactor();
                int cornerX = (1 + boardPos.x + 7 + selectedPiece.getX() * 26) * sf;
                int cornerY = (1 + boardPos.y + 7 + selectedPiece.getY() * 26) * sf;

                gc.fillRect(cornerX, cornerY, 23 * sf, sf);
                gc.fillRect(cornerX, cornerY + (22 * sf), 23 * sf, sf);

                gc.fillRect(cornerX, cornerY, sf, 22 * sf);
                gc.fillRect(cornerX + (sf * 22), cornerY, sf, 22 * sf);
            }
        }));
    }

    private Position centerWithOffset(int xOffset, int y) {
        int center = drawingContextProvider.getDrawingContext().getWidth() / 2;
        return new Position(Anchor.TOP_CENTER, false, false, center + xOffset, y);
    }

    private TextButton createMovementButton(Direction d, Position p) {
        TextButton textButton = new TextButton(d.displayName, p, drawingContextProvider)
                .withAction(() -> {
                    if (selectedPiece != null) {
                        gameLifecycleManager.getBoard().submitTurn(selectedPiece.getAction(d, gameLifecycleManager.getBoard()));
                        selectedPiece = null;
                        updateValidMoves();
                        updatePlayerTurnText();
                    }
                });

        textButton.setDisabled(true);
        mouseClickEventDelegator.addHandler(textButton.getMouseClickListener());
        mouseMoveEventDelegator.addHandler(textButton.getMouseMoveListener());
        drawables.add(textButton);
        return textButton;
    }

    private void setSelectedPiece(Piece p) {
        selectedPiece = p;
        updateValidMoves();
    }

    private void updateValidMoves() {
        setTextButtonDisabled(Direction.UP, up);
        setTextButtonDisabled(Direction.UP_LEFT, upLeft);
        setTextButtonDisabled(Direction.UP_RIGHT, upRight);
        setTextButtonDisabled(Direction.DOWN, down);
        setTextButtonDisabled(Direction.DOWN_LEFT, downLeft);
        setTextButtonDisabled(Direction.DOWN_RIGHT, downRight);
        setTextButtonDisabled(Direction.LEFT, left);
        setTextButtonDisabled(Direction.RIGHT, right);

        if (selectedPiece != null) {
            rotateCW.setDisabled(false);
            rotateAntiCW.setDisabled(false);
        } else {
            rotateCW.setDisabled(true);
            rotateAntiCW.setDisabled(true);
        }
    }

    private void setTextButtonDisabled(Direction d, TextButton textButton) {
        boolean disabled = true;

        if (selectedPiece != null) {
            int newX = selectedPiece.getX() + d.x, newY = selectedPiece.getY() + d.y;

            if (gameLifecycleManager.getBoard().isLegalMove(selectedPiece, newX, newY)) {
                disabled = false;
            }
        }

        textButton.setDisabled(disabled);
    }

    private void updatePlayerTurnText() {
        String player;

        if (gameLifecycleManager.getBoard().getPlayerTurn() == Piece.PieceColor.WHITE) {
            player = "white";
        } else {
            player = "red";
        }

        playerTurnText.setText(String.format("%s's turn", player));
    }

    @Override
    public void onSelected() {
    }

    @Override
    public void setHandlers(Canvas canvas) {
        canvas.setOnMouseClicked(mouseClickEventDelegator::handle);
        canvas.setOnMouseMoved(mouseMoveEventDelegator::handle);
    }

    @Override
    public void clearHandlers(Canvas canvas) {
    }

    @Override
    public List<Drawable> getDrawables() {
        return drawables;
    }

    @Override
    public List<Updatable> getUpdatables() {
        return updatables;
    }
}
