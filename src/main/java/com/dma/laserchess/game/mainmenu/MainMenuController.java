package com.dma.laserchess.game.mainmenu;

import com.dma.laserchess.game.engine.ScreenViewFactory;
import com.dma.laserchess.game.engine.ScreenViewSelector;

public class MainMenuController {
    private final ScreenViewSelector screenViewSelector;
    private final ScreenViewFactory screenViewFactory;

    public MainMenuController(ScreenViewSelector screenViewSelector, ScreenViewFactory screenViewFactory) {
        this.screenViewSelector = screenViewSelector;
        this.screenViewFactory = screenViewFactory;
    }

    public void startGame() {
        screenViewSelector.setCurrentScreen(screenViewFactory.createBoardScreen());
    }
}
