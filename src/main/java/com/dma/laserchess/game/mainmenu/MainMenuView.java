package com.dma.laserchess.game.mainmenu;

import com.dma.laserchess.game.engine.DrawingContextProvider;
import com.dma.laserchess.game.engine.MenuScreenView;
import com.dma.laserchess.game.engine.components.Position;
import com.dma.laserchess.game.engine.components.Text;
import com.dma.laserchess.game.engine.components.TextButton;
import javafx.scene.paint.Color;

public class MainMenuView extends MenuScreenView {
    private final MainMenuController mainMenuController;
    private final DrawingContextProvider drawingContextProvider;

    public MainMenuView(MainMenuController mainMenuController, DrawingContextProvider drawingContextProvider) {
        this.mainMenuController = mainMenuController;
        this.drawingContextProvider = drawingContextProvider;

        addDrawable((gc, updateCount, dc) -> {
            gc.setFill(Color.WHITE);
            gc.fillRect(0, 0, dc.getScreenWidth(), dc.getScreenHeight());
        });

        addDrawable(new Text("laser chess", Position.centeredX(20)));

        addMouseComponent(new TextButton("play", Position.centeredX(40), drawingContextProvider)
                .withAction(mainMenuController::startGame));
    }
}
