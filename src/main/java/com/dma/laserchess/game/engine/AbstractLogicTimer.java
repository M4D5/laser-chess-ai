package com.dma.laserchess.game.engine;

/**
 * This class represents a thread that will run all logic at
 * roughly 60 updates per second, and can be used to update
 * game logic at a consistent rate, not tied to the framerate.
 */
public abstract class AbstractLogicTimer extends Thread {
    private boolean active = true;
    private long updateCount = 0;

    public long getUpdateCount() {
        return updateCount;
    }

    @Override
    public void run() {
        while (active) {
            if (shouldUpdate()) {
                updateCount++;
                update();
            }

            try {
                Thread.sleep(1000 / 60); // Update 60 times per second
            } catch (InterruptedException e) {
                // We are not expecting an interrupt, so the exception is rethrown as a RuntimeException
                throw new RuntimeException("The logic thread was interrupted", e);
            }
        }
    }

    /**
     * Subclasses can override this function to prevent the timer from
     * updating, should the state not warrant updates.
     *
     * @return a boolean, determining if the thread should call update.
     */
    protected boolean shouldUpdate() {
        return true;
    }

    protected abstract void update();

    /**
     * The thread won't stop unless it's told to, and as a result it will
     * keep the program open even if the JavaFX thread is stopped. Calling this
     * method will end the execution of the thread after the next update.
     */
    public void deactivate() {
        this.active = false;
    }
}
