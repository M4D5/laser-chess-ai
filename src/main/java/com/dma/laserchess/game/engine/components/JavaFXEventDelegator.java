package com.dma.laserchess.game.engine.components;

import com.dma.laserchess.game.engine.EventDelegator;
import javafx.event.Event;
import javafx.event.EventHandler;

public class JavaFXEventDelegator<T extends Event> extends EventDelegator<EventHandler<T>, T> implements EventHandler<T> {
    @Override
    protected void delegateEvent(EventHandler<T> eventHandler, T event) {
        eventHandler.handle(event);
    }
}
