package com.dma.laserchess.game.engine;

public class DrawingContextProvider {
    private static final DrawingContext drawingContext = new DrawingContext(300, 300, 3);

    public DrawingContext getDrawingContext() {
        return drawingContext;
    }
}
