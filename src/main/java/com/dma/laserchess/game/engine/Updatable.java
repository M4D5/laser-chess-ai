package com.dma.laserchess.game.engine;

public interface Updatable {
    void update(long updateCount);
}
