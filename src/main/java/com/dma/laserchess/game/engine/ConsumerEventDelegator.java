package com.dma.laserchess.game.engine;

import java.util.function.Consumer;

public class ConsumerEventDelegator<T> extends EventDelegator<Consumer<T>, T> {
    @Override
    protected void delegateEvent(Consumer<T> eventHandler, T event) {
        eventHandler.accept(event);
    }
}
