package com.dma.laserchess.game.engine;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class EventDelegator<T, E> {
    private final Set<T> eventHandlers = Collections.synchronizedSet(new HashSet<>());

    public void clear() {
        eventHandlers.clear();
    }

    public void addHandler(T handler) {
        eventHandlers.add(handler);
    }

    public void removeHandler(T handler) {
        eventHandlers.remove(handler);
    }

    public void handle(E event) {
        for(T handler : eventHandlers) {
            delegateEvent(handler, event);
        }
    }

    protected abstract void delegateEvent(T eventHandler, E event);
}
