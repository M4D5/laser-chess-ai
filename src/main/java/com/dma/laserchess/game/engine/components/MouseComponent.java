package com.dma.laserchess.game.engine.components;

import com.dma.laserchess.game.engine.Drawable;

public interface MouseComponent extends MouseListener, Drawable {
}
