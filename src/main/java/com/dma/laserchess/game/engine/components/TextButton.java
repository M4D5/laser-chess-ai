package com.dma.laserchess.game.engine.components;

import com.dma.laserchess.game.engine.DrawingContextProvider;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import lombok.Setter;

import java.awt.*;

public class TextButton extends Text implements MouseClickListener, MouseMoveListener, MouseComponent {
    private final DrawingContextProvider drawingContextProvider;

    private Runnable action = () -> {
    };

    private boolean hoverActive;
    @Setter
    private boolean disabled;

    private EventHandler<MouseEvent> mouseClickListener = event -> {
        Rectangle bounds = getScaledBounds();

        if (!disabled && event.getX() > bounds.x &&
                event.getX() < bounds.x + bounds.width &&
                event.getY() > bounds.y &&
                event.getY() < bounds.y + bounds.height) {
            this.action.run();
        }
    };

    private EventHandler<MouseEvent> mouseMoveListener = event -> {
        Rectangle bounds = getScaledBounds();

        if (event.getX() > bounds.x &&
                event.getX() < bounds.x + bounds.width &&
                event.getY() > bounds.y &&
                event.getY() < bounds.y + bounds.height) {
            hoverActive = true;
        } else {
            hoverActive = false;
        }
    };


    public TextButton(String text, Position position, DrawingContextProvider drawingContextProvider) {
        super(text, position);
        this.drawingContextProvider = drawingContextProvider;
    }

    public TextButton withAction(Runnable action) {
        this.action = action;
        return this;
    }

    private Rectangle getScaledBounds() {
        Rectangle bounds = getBounds(drawingContextProvider.getDrawingContext());
        int sf = drawingContextProvider.getDrawingContext().getScalingFactor();
        return new Rectangle(bounds.x * sf, bounds.y * sf, bounds.width * sf, bounds.height * sf);
    }

    @Override
    public EventHandler<MouseEvent> getMouseClickListener() {
        return mouseClickListener;
    }

    @Override
    public EventHandler<MouseEvent> getMouseMoveListener() {
        return mouseMoveListener;
    }

    @Override
    protected Color getColor() {
        if(disabled) {
            return Color.web("#555555");
        }

        if (hoverActive) {
            return Color.web("#ffffb3");
        } else {
            return Color.web("#ffffff");
        }
    }
}
