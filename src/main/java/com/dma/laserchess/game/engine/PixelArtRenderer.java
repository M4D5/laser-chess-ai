package com.dma.laserchess.game.engine;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import lombok.Data;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class PixelArtRenderer {
    private static final Map<CacheKey, WritableImage> scaleBuffer = new HashMap<>();

    @Data
    private static class CacheKey {
        private final Image image;
    }

    public static void render(Image image, Point position, DrawingContext dc, GraphicsContext gc) {
        render(image, position, 0, dc, gc);
    }

    public static void render(Image image, Point position, int rotationDegs, DrawingContext dc, GraphicsContext gc) {

        PixelReader pixelReader = image.getPixelReader();
        int sf = dc.getScalingFactor();
        int w = image.widthProperty().intValue(), h = image.heightProperty().intValue();

        WritableImage scaledImage = scaleBuffer.get(new CacheKey(image));

        if (scaledImage == null) {
            int[] intBuffer = new int[w * sf * h * sf];

            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    int argb = pixelReader.getArgb(x, y);

                    int x1 = x * sf, y1 = y * sf;

                    for (int c1 = 0; c1 < sf; c1++) {
                        for (int c2 = 0; c2 < sf; c2++) {
                            intBuffer[(x1 + c1) + (w * sf * (y1 + c2))] = argb;
                        }
                    }
                }
            }


            byte[] buffer = new byte[intBuffer.length * 4];

            for (int i = 0; i < intBuffer.length; i++) {
                buffer[i * 4] = (byte) intBuffer[i];
                buffer[i * 4 + 1] = (byte) (intBuffer[i] >> 8);
                buffer[i * 4 + 2] = (byte) (intBuffer[i] >> 16);
                buffer[i * 4 + 3] = (byte) (intBuffer[i] >> 24);
            }

            scaledImage = new WritableImage(w * sf, h * sf);
            scaledImage.getPixelWriter().setPixels(
                    0,
                    0,
                    w * sf,
                    h * sf,
                    PixelFormat.getByteBgraPreInstance(),
                    buffer,
                    0,
                    w * sf * 4);

            scaleBuffer.put(new CacheKey(image), scaledImage);
        }

        double x = position.getX() * sf, y = position.getY() * sf;
        double halfWidthX = scaledImage.getWidth() / 2.0, halfWidthY = scaledImage.getHeight() / 2.0;
        gc.translate(halfWidthX + x, halfWidthY + y);
        gc.rotate(rotationDegs);
        gc.drawImage(scaledImage, -halfWidthX, -halfWidthY);
        gc.rotate(-rotationDegs);
        gc.translate(-x - halfWidthX, -y - halfWidthY);
    }
}