package com.dma.laserchess.game.engine;

import java.util.List;

/**
 * This class updates updatables provided by the ScreenViewSelector
 * specified in the constructor.
 */
public class ScreenViewLogicTimer extends AbstractLogicTimer {
    private final ScreenViewSelector screenViewSelector;

    public ScreenViewLogicTimer(ScreenViewSelector screenViewSelector) {
        this.screenViewSelector = screenViewSelector;
    }

    @Override
    protected void update() {
        final List<Updatable> updatables = screenViewSelector.getUpdatables();
        for (Updatable u : updatables) {
            u.update(getUpdateCount());
        }
    }
}
