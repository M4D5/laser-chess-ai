package com.dma.laserchess.game.engine.components;


import com.dma.laserchess.game.engine.Drawable;
import com.dma.laserchess.game.engine.DrawingContext;
import com.dma.laserchess.game.engine.PixelArtRenderer;
import javafx.scene.canvas.GraphicsContext;
import lombok.Setter;

import java.awt.*;

public class ImageAsset implements Drawable {
    private final javafx.scene.image.Image image;

    @Setter
    private Position position;

    public ImageAsset(javafx.scene.image.Image image, Position position) {
        this.image = image;
        this.position = position;
    }

    private Rectangle getBounds(DrawingContext dc) {
        Point offset = Anchor.anchorOffset(position.getAnchor(),
                new Dimension(image.widthProperty().intValue(), image.heightProperty().intValue()));

        return new Rectangle(
                position.getX(dc) + offset.x,
                position.getY(dc) + offset.y,
                image.widthProperty().intValue(),
                image.heightProperty().intValue());
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        Rectangle bounds = getBounds(dc);
        PixelArtRenderer.render(image, new Point(bounds.x, bounds.y), 0, dc, gc);
    }
}
