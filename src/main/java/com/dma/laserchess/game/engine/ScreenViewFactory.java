package com.dma.laserchess.game.engine;


import com.dma.laserchess.game.board.BoardScreen;
import com.dma.laserchess.game.logic.GameLifecycleManager;
import com.dma.laserchess.game.mainmenu.MainMenuController;
import com.dma.laserchess.game.mainmenu.MainMenuView;
import lombok.RequiredArgsConstructor;

/**
 * This class is responsible for creating all views, and provides
 * views with everything they need to switch views.
 */
@RequiredArgsConstructor
public class ScreenViewFactory {
    private final DrawingContextProvider drawingContextProvider;
    private final ScreenViewSelector screenViewSelector;
    private final GameLifecycleManager gameLifecycleManager;

    public MainMenuView createMainMenuView() {
        MainMenuController mainMenuController = new MainMenuController(screenViewSelector, this);
        return new MainMenuView(mainMenuController, drawingContextProvider);
    }

    public ScreenView createBoardScreen() {
        return new BoardScreen(gameLifecycleManager, drawingContextProvider);
    }
}
