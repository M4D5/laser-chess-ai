# Laser Chess AI
MCTS implementation against the game Laser Chess, written in Java 11. 
To run the game (on Windows), use the provided bat file.

Otherwise, requires JavaFX libraries to be installed for the jvm (can be downloaded here: https://gluonhq.com/products/javafx/)

## AI Implementation
All logic relating to the AI implementation is found in the class com.dma.laserchess.ai.AI.